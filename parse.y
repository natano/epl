/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <sys/types.h>
#include <sys/queue.h>

#include <err.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "expr.h"

int	yylex(void);
int	yyparse(void);
int	yyerror(const char *, ...);

static struct expr	*expr(enum etype);
static struct expr	*callattr(struct expr *, const char *, int, ...);

struct expr	*parse(FILE *);
struct expr	*parse_str(const char *);

static struct expr *result;
%}

%union {
	int		 i;
	float		 f;
	struct {
		char	*buf;
		size_t	 len;
	} str;
	char		*ident;
	struct expr	*expr;
	struct {
		int	 arity;
		char	**argnames;
	} args;
	struct {
		int		 argc;
		struct expr	**argv;
	} exprs;
}

%token KW_FUNC KW_IF KW_ELSE KW_NIL KW_TRUE KW_FALSE KW_NEW
%token OP_EQ OP_NE
%token <i> INT
%token <f> FLOAT
%token <str> STR
%token <ident> IDENT

%type <expr> expr block statement statements
%type <args> args_or_empty args
%type <exprs> exprs exprs_or_empty

%right IF KW_ELSE
%right '='
%right TERNARY '?'
%left OP_EQ OP_NE
%left '+' '-'
%left '*' '/' '%'
%right UNARY
%left CALL '(' '.'

%%

program	: statements {
		result = $1;
	}
	;

args_or_empty
	: /* empty */ {
		$$.arity = 0;
		$$.argnames = NULL;
	}
	| args
	;

args	: IDENT {
		$$.arity = 1;
		$$.argnames = calloc(1, sizeof(char *));
		$$.argnames[0] = $1;
	}
	| args ',' IDENT {
		char **tmp;

		$$ = $1;

		/* XXX: check for argc overflow */
		tmp = reallocarray($$.argnames, $$.arity + 1, sizeof(char *));
		if (tmp == NULL)
			err(1, "reallocarray");

		$$.argnames = tmp;
		$$.argnames[$$.arity++] = $3;
	}
	;

exprs_or_empty
	: /* empty */ {
		$$.argc = 0;
		$$.argv = NULL;
	}
	| exprs
	;

exprs	: expr {
		$$.argc = 1;
		$$.argv = calloc(1, sizeof(struct expr *));
		$$.argv[0] = $1;
	}
	| exprs ',' expr {
		struct expr **tmp;

		$$ = $1;

		/* XXX: check for argc overflow */
		tmp = reallocarray($$.argv, $$.argc + 1, sizeof(struct expr *));
		if (tmp == NULL)
			err(1, "reallocarray");

		$$.argv = tmp;
		$$.argv[$$.argc++] = $3;
	}
	;

expr	: KW_NIL {
		$$ = expr(EXPR_NIL);
	}
	| KW_TRUE {
		$$ = expr(EXPR_BOOL);
		$$->data.b = true;
	}
	| KW_FALSE {
		$$ = expr(EXPR_BOOL);
		$$->data.b = false;
	}
	| INT {
		$$ = expr(EXPR_INT);
		$$->data.i = $1;
	}
	| FLOAT {
		$$ = expr(EXPR_FLOAT);
		$$->data.f = $1;
	}
	| STR {
		$$ = expr(EXPR_STR);
		$$->data.str.buf = $1.buf;
		$$->data.str.len = $1.len;
	}
	| IDENT {
		$$ = expr(EXPR_IDENT);
		$$->data.ident = $1;
	}
	| '(' expr ')' {
		$$ = $2;
	}
	| KW_FUNC '(' args_or_empty ')' block {
		$$ = expr(EXPR_FUNC);
		$$->data.func.arity = $3.arity;
		$$->data.func.argnames = $3.argnames;
		$$->data.func.code = $5;
	}
	| expr '(' exprs_or_empty ')' %prec CALL {
		if ($1->type == EXPR_GETATTR) {
			$$ = expr(EXPR_CALLATTR);
			$$->data.callattr.obj = $1->data.getattr.obj;
			$$->data.callattr.attrname = $1->data.getattr.attrname;
			$$->data.callattr.argc = $3.argc;
			$$->data.callattr.argv = $3.argv;
			free($1);
		} else {
			$$ = expr(EXPR_CALL);
			$$->data.call.func = $1;
			$$->data.call.argc = $3.argc;
			$$->data.call.argv = $3.argv;
		}
	}
	| expr '+' expr {
		$$ = callattr($1, "__add__", 1, $3);
	}
	| expr '-' expr {
		$$ = callattr($1, "__sub__", 1, $3);
	}
	| expr '*' expr {
		$$ = callattr($1, "__mul__", 1, $3);
	}
	| expr '/' expr {
		$$ = callattr($1, "__div__", 1, $3);
	}
	| expr '%' expr {
		$$ = callattr($1, "__mod__", 1, $3);
	}
	| '+' expr %prec UNARY {
		$$ = callattr($2, "__pos__", 0);
	}
	| '-' expr %prec UNARY {
		$$ = callattr($2, "__neg__", 0);
	}
	| expr '?' expr ':' expr %prec TERNARY {
		$$ = expr(EXPR_IFELSE);
		$$->data.ifelse.cond = $1;
		$$->data.ifelse.branch1 = $3;
		$$->data.ifelse.branch2 = $5;
	}
	| IDENT '=' expr {
		$$ = expr(EXPR_ASSIGN);
		$$->data.assign.name = $1;
		$$->data.assign.val = $3;
	}
	| expr OP_EQ expr {
		$$ = callattr($1, "__eq__", 1, $3);
	}
	| expr OP_NE expr {
		$$ = callattr($1, "__ne__", 1, $3);
	}
	| KW_NEW {
		$$ = expr(EXPR_NEW);
	}
	| expr '.' IDENT '=' expr {
		$$ = expr(EXPR_SETATTR);
		$$->data.setattr.obj = $1;
		$$->data.setattr.attrname = $3;
		$$->data.setattr.val = $5;
	}
	| expr '.' IDENT {
		$$ = expr(EXPR_GETATTR);
		$$->data.getattr.obj = $1;
		$$->data.getattr.attrname = $3;
	}
	;

block	: '{' statements '}' {
		$$ = $2;
	}
	;

statements
	: statement {
		$$ = expr(EXPR_BLOCK);
		TAILQ_INSERT_HEAD(&$$->data.block, $1, entry);
	}
	| statements statement {
		$$ = $1;
		TAILQ_INSERT_TAIL(&$$->data.block, $2, entry);
	}
	;

statement
	: expr ';'
	| block
	| KW_IF '(' expr ')' statement %prec IF {
		$$ = expr(EXPR_IFELSE);
		$$->data.ifelse.cond = $3;
		$$->data.ifelse.branch1 = $5;
		$$->data.ifelse.branch2 = NULL;
	}
	| KW_IF '(' expr ')' statement KW_ELSE statement %prec IF {
		$$ = expr(EXPR_IFELSE);
		$$->data.ifelse.cond = $3;
		$$->data.ifelse.branch1 = $5;
		$$->data.ifelse.branch2 = $7;
	}
	;

%%

int
yyerror(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	(void)vfprintf(stderr, fmt, ap);
	(void)fprintf(stderr, "\n");
	va_end(ap);
	return 0;
}

static struct expr *
expr(enum etype type)
{
	struct expr *ep;

	ep = calloc(1, sizeof(struct expr));
	if (ep == NULL)
		err(1, "malloc");

	ep->type = type;
	return ep;
}
static struct expr *
callattr(struct expr *obj, const char *attrname, int argc, ...)
{
	struct expr *ep;
	va_list ap;
	int i;

	ep = expr(EXPR_CALLATTR);
	ep->data.callattr.obj = obj;

	ep->data.callattr.attrname = strdup(attrname);
	if (ep->data.callattr.attrname == NULL)
		err(1, "strdup");

	ep->data.callattr.argc = argc;

	va_start(ap, argc);
	if (argc > 0) {
		ep->data.callattr.argv = calloc(argc, sizeof(struct expr *));
		if (ep->data.callattr.argv == NULL)
			err(1, "malloc");
		for (i = 0; i < argc; i++)
			ep->data.callattr.argv[0] = va_arg(ap, struct expr *);
	} else {
		ep->data.callattr.argv = NULL;
	}
	va_end(ap);

	return ep;
}

struct expr *
parse(FILE *f)
{
	extern FILE *yyin;

	yyin = f;
	if (yyparse() != 0)
		return NULL;
	return result;
}

struct expr *
parse_str(const char *s)
{
	FILE *f;
	struct expr *res;

	f = fmemopen((void *)s, strlen(s), "r");
	if (f == NULL)
		return NULL;

	res = parse(f);

	(void)fclose(f);
	return res;
}
