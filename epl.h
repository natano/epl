/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef EPL_H
#define EPL_H

struct node {
	RB_ENTRY(node)	 entry;
	char		*name;
	struct obj	*obj;
};
RB_HEAD(nodetree, node);


enum objtype {
	OBJ_PLAIN, OBJ_NIL, OBJ_BOOL, OBJ_INT, OBJ_FLOAT, OBJ_STR, OBJ_FUNC,
	OBJ_CFUNC, OBJ_SCOPE,
};

struct ctx {
	struct obj	*global;
	struct obj	*obj_proto;
	struct obj	*nil_proto;
	struct obj	*bool_proto;
	struct obj	*int_proto;
	struct obj	*float_proto;
	struct obj	*str_proto;
	struct obj	*func_proto;
	struct obj	*nil_obj;
	struct obj	*true_obj;
	struct obj	*false_obj;
	char		 err[128];
};

struct obj {
	enum objtype	type;
	size_t		ref;
	struct nodetree	attrs;
	union {
		_Bool	b;
		int	i;
		float	f;
		struct {
			char	*buf;
			size_t	 len;
		} str;
		struct {
			int		 arity;
			char		**argnames;
			struct expr	*code;
			struct obj	*scope;
		} func;
		struct obj	*(*cfunc)(struct ctx *,
				    struct obj *, struct obj *,
				    int, struct expr **);
		struct {
			struct nodetree	 nodes;
			struct obj	*parent;
		} scope;
	} data;

	/*
	 * The following members are used by the garbage collector. They are
	 * included in struct obj, so allocating additional memory during
	 * garbage collector runs can be avoided.
	 */
	RB_ENTRY(obj)	gc_entry;
	size_t		gc_ref;
};

#define obj_ref(o)	((o)->ref++, (o))

struct proto_op {
	const char	*name;
	struct obj	*(*fn)(struct ctx *, struct obj *, struct obj *, int,
			    struct expr **);
};

extern const struct proto_op obj_ops[];
extern const struct proto_op bool_ops[];
extern const struct proto_op num_ops[];
extern const struct proto_op str_ops[];

/* parse.y */
struct expr	*parse(FILE *);
struct expr	*parse_str(const char *);

/* epl.c */
struct obj	*scope(struct obj *);
int		 scope_add(struct obj *, const char *, struct obj *);
struct obj	*scope_lookup(struct obj *, const char *);

struct obj	*object(enum objtype, struct obj *);
void		 obj_unref(struct obj *);
void		 obj_free(struct obj *);
int		 obj_setattr(struct obj *, const char *, struct obj *);
struct obj	*obj_getattr(struct obj *, const char *);
struct obj	*obj_callattr(struct ctx *, struct obj *, struct obj *,
		    const char *, int, struct expr **);
void		 print_obj(struct obj *);

int nodecmp(struct node *, struct node *);
RB_PROTOTYPE(nodetree, node, entry, nodecmp);

struct obj	*build_proto(const struct proto_op *, struct obj *);

struct ctx	*ctx_new(void);
void		 ctx_free(struct ctx *);
void		 ctx_seterr(struct ctx *, const char *, ...);

struct obj	*eval_expr(struct ctx *, struct expr *, struct obj *);

/* gc.c */
void	gc_collect(struct obj *);

#endif /* !EPL_H */
