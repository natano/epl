/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/tree.h>

#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "epl.h"
#include "expr.h"

static struct obj	*num_add(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_sub(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_mul(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_div(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_mod(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_pos(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_neg(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_eq(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*num_ne(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);

const struct proto_op num_ops[] = {
	{"__add__", num_add},
	{"__sub__", num_sub},
	{"__mul__", num_mul},
	{"__div__", num_div},
	{"__mod__", num_mod},
	{"__pos__", num_pos},
	{"__neg__", num_neg},
	{"__eq__", num_eq},
	{"__ne__", num_ne},
	{NULL, NULL},
};

#define ARITH(name, op)							\
static struct obj *							\
num_##name(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,	\
    struct expr **argv)							\
{									\
	struct obj *other, *res;					\
									\
	if (obj == NULL) {						\
		ctx_seterr(ctx, "expected object");			\
		return NULL;						\
	} else if (argc != 1) {						\
		ctx_seterr(ctx, "wrong number of arguments");		\
		return NULL;						\
	}								\
									\
	other = eval_expr(ctx, argv[0], s);				\
	if (other == NULL)						\
		return NULL;						\
									\
	if (obj->type == OBJ_INT && other->type == OBJ_INT) {		\
		res = object(OBJ_INT, ctx->int_proto);			\
		if (res == NULL) {					\
error:									\
			obj_unref(other);				\
			ctx_seterr(ctx, "%s", strerror(errno));		\
			return NULL;					\
		}							\
		res->data.i = obj->data.i op other->data.i;		\
	} else if (obj->type == OBJ_INT && other->type == OBJ_FLOAT) {	\
		res = object(OBJ_FLOAT, ctx->float_proto);		\
		if (res == NULL)					\
			goto error;					\
		res->data.f = obj->data.i op other->data.f;		\
	} else if (obj->type == OBJ_FLOAT && other->type == OBJ_INT) {	\
		res = object(OBJ_FLOAT, ctx->float_proto);		\
		if (res == NULL)					\
			goto error;					\
		res->data.f = obj->data.f op other->data.i;		\
	} else if (obj->type == OBJ_FLOAT && other->type == OBJ_FLOAT) {\
		res = object(OBJ_FLOAT, ctx->float_proto);		\
		if (res == NULL)					\
			goto error;					\
		res->data.f = obj->data.f op other->data.f;		\
	} else {							\
		obj_unref(other);					\
		ctx_seterr(ctx, "expected int or float");		\
		return NULL;						\
	}								\
									\
	obj_unref(other);						\
	return res;							\
}

ARITH(add, +)
ARITH(sub, -)
ARITH(mul, *)
ARITH(div, /)

static struct obj *
num_mod(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,
    struct expr **argv)
{
	struct obj *other, *res;

	if (obj == NULL) {
		ctx_seterr(ctx, "expected object");
		return NULL;
	} else if (argc != 1) {
		ctx_seterr(ctx, "wrong number of arguments");
		return NULL;
	}

	other = eval_expr(ctx, argv[0], s);
	if (other == NULL)
		return NULL;

	if (obj->type == OBJ_INT && other->type == OBJ_INT) {
		res = object(OBJ_INT, ctx->int_proto);
		if (res == NULL) {
error:
			obj_unref(other);
			ctx_seterr(ctx, "%s", strerror(errno));
			return NULL;
		}
		res->data.i = obj->data.i % other->data.i;
	} else if (obj->type == OBJ_INT && other->type == OBJ_FLOAT) {
		res = object(OBJ_FLOAT, ctx->float_proto);
		if (res == NULL)
			goto error;
		res->data.f = fmodf(obj->data.i, other->data.f);
	} else if (obj->type == OBJ_FLOAT && other->type == OBJ_INT) {
		res = object(OBJ_FLOAT, ctx->float_proto);
		if (res == NULL)
			goto error;
		res->data.f = fmodf(obj->data.f, other->data.i);
	} else if (obj->type == OBJ_FLOAT && other->type == OBJ_FLOAT) {
		res = object(OBJ_FLOAT, ctx->float_proto);
		if (res == NULL)
			goto error;
		res->data.f = fmodf(obj->data.f, other->data.f);
	} else {
		obj_unref(other);
		ctx_seterr(ctx, "expected int or float");
		return NULL;
	}

	obj_unref(other);
	return res;
}

#define UNARY(name, op)							\
static struct obj *							\
num_##name(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,	\
    struct expr **argv)							\
{									\
	struct obj *res;						\
									\
	if (obj == NULL) {						\
		ctx_seterr(ctx, "expected object");			\
		return NULL;						\
	} else if (argc != 0) {						\
		ctx_seterr(ctx, "wrong number of arguments");		\
		return NULL;						\
	}								\
									\
	switch (obj->type) {						\
	case OBJ_INT:							\
		res = object(OBJ_INT, ctx->int_proto);			\
		if (res == NULL) {					\
			ctx_seterr(ctx, "%s", strerror(errno));		\
			return NULL;					\
		}							\
		res->data.i = op obj->data.i;				\
		break;							\
	case OBJ_FLOAT:							\
		res = object(OBJ_FLOAT, ctx->float_proto);		\
		if (res == NULL) {					\
			ctx_seterr(ctx, "%s", strerror(errno));		\
			return NULL;					\
		}							\
		res->data.f = op obj->data.f;				\
		break;							\
	default:							\
		ctx_seterr(ctx, "expected int or float");		\
		return NULL;						\
	}								\
	return res;							\
}

UNARY(pos, +);
UNARY(neg, -);

#define COMPARISON(name, op)						\
static struct obj *							\
num_##name(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,	\
    struct expr **argv)							\
{									\
	struct obj *other;						\
	bool ret;							\
									\
	if (obj == NULL) {						\
		ctx_seterr(ctx, "expected object");			\
		return NULL;						\
	} else if (argc != 1) {						\
		ctx_seterr(ctx, "wrong number of arguments");		\
		return NULL;						\
	}								\
									\
	other = eval_expr(ctx, argv[0], s);				\
	if (other == NULL)						\
		return NULL;						\
									\
	if (obj->type == OBJ_INT && other->type == OBJ_INT)		\
		ret = obj->data.i op other->data.i;			\
	else if (obj->type == OBJ_INT && other->type == OBJ_FLOAT)	\
		ret = obj->data.i op other->data.f;			\
	else if (obj->type == OBJ_FLOAT && other->type == OBJ_INT)	\
		ret = obj->data.f op other->data.i;			\
	else if (obj->type == OBJ_FLOAT && other->type == OBJ_FLOAT)	\
		ret = obj->data.f op other->data.f;			\
	else								\
		ret = true op true;					\
									\
	obj_unref(other);						\
	return obj_ref(ret ? ctx->true_obj : ctx->false_obj);		\
}

COMPARISON(eq, ==)
COMPARISON(ne, !=)
