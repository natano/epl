PROG=	epl
SRCS=	epl.c gc.c obj_proto.c bool_proto.c num_proto.c str_proto.c parse.y lex.l
NOMAN=	1
COPTS=	-std=c99 -I${.CURDIR:Q}
#COPTS+=	-g -ggdb -O0
COPTS+=	-Wall -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations
COPTS+=	-Wwrite-strings -Wsign-compare
LDADD=	-lm

.include <bsd.prog.mk>
