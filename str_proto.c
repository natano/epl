/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/tree.h>

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "epl.h"
#include "expr.h"

static struct obj	*str_add(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*str_mul(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*str_eq(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*str_ne(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);

const struct proto_op str_ops[] = {
	{"__add__", str_add},
	{"__mul__", str_mul},
	{"__eq__", str_eq},
	{"__ne__", str_ne},
	{NULL, NULL},
};

static struct obj *
str_add(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,
    struct expr **argv)
{
	struct obj *other, *res;

	if (obj == NULL || obj->type != OBJ_STR) {
		ctx_seterr(ctx, "expected str");
		return NULL;
	} else if (argc != 1) {
		ctx_seterr(ctx, "wrong number of arguments");
		return NULL;
	}

	other = eval_expr(ctx, argv[0], s);
	if (other == NULL) {
		return NULL;
	} else if (other->type != OBJ_STR) {
		obj_unref(other);
		ctx_seterr(ctx, "expected str");
		return NULL;
	}

	res = object(OBJ_STR, ctx->str_proto);
	if (res == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		obj_unref(other);
		return NULL;
	}

	res->data.str.buf = malloc(obj->data.str.len + other->data.str.len);
	if (res->data.str.buf == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		obj_unref(other);
		obj_unref(res);
		return NULL;
	}
	(void)memcpy(res->data.str.buf, obj->data.str.buf, obj->data.str.len);
	(void)memcpy(res->data.str.buf + obj->data.str.len, other->data.str.buf,
	    other->data.str.len);
	res->data.str.len = obj->data.str.len + other->data.str.len;

	obj_unref(other);
	return res;
}

static struct obj *
str_mul(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,
    struct expr **argv)
{
	struct obj *other, *res;
	char *buf;
	int n;

	if (obj == NULL || obj->type != OBJ_STR) {
		ctx_seterr(ctx, "expected str");
		return NULL;
	} else if (argc != 1) {
		ctx_seterr(ctx, "wrong number of arguments");
		return NULL;
	}

	other = eval_expr(ctx, argv[0], s);
	if (other == NULL) {
		return NULL;
	} else if (other->type != OBJ_INT) {
		obj_unref(other);
		ctx_seterr(ctx, "expected int");
		return NULL;
	}

	n = other->data.i;
	obj_unref(other);

	if (n < 0)
		n = 0;

	if (n > 0 && SIZE_MAX / (size_t)n < obj->data.str.len) {
		ctx_seterr(ctx, "resulting string is too large");
		return NULL;
	}

	res = object(OBJ_STR, ctx->str_proto);
	if (res == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		return NULL;
	}

	res->data.str.buf = malloc(obj->data.str.len * (size_t)n);
	if (res->data.str.buf == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		obj_unref(res);
		return NULL;
	}
	for (buf = res->data.str.buf; n > 0; n--) {
		(void)memcpy(buf, obj->data.str.buf,
		    obj->data.str.len);
		buf += obj->data.str.len;
	}
	res->data.str.len = obj->data.str.len * (size_t)n;
	return res;
}

#define COMPARISON(name, op)						\
static struct obj *							\
str_##name(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,	\
    struct expr **argv)							\
{									\
	struct obj *other;						\
	bool ret;							\
									\
	if (obj == NULL || obj->type != OBJ_STR) {			\
		ctx_seterr(ctx, "expected str");			\
		return NULL;						\
	} else if (argc != 1) {						\
		ctx_seterr(ctx, "wrong number of arguments");		\
		return NULL;						\
	}								\
									\
	other = eval_expr(ctx, argv[0], s);				\
	if (other == NULL)						\
		return NULL;						\
									\
	if (obj->type == OBJ_STR) {					\
		ret = obj->data.str.len == other->data.str.len &&	\
		    memcmp(obj->data.str.buf, other->data.str.buf,	\
		    obj->data.str.len) == 0;				\
	} else {							\
		ret = true op true;					\
	}								\
									\
	obj_unref(other);						\
	return obj_ref(ret ? ctx->true_obj : ctx->false_obj);		\
}

COMPARISON(eq, ==)
COMPARISON(ne, !=)
