/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/tree.h>

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "epl.h"
#include "expr.h"

static struct obj	*eval_int(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_float(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_str(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_ident(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_nil(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_bool(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_func(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_call(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_block(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_ifelse(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_assign(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_new(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_getattr(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_setattr(struct ctx *, struct expr *, struct obj *);
static struct obj	*eval_callattr(struct ctx *, struct expr *, struct obj *);

int
main(int argc, char *argv[])
{
	struct ctx *ctx;
	FILE *f;
	const char *fname;
	struct expr *expr;
	struct obj *res;

	if (argc == 1 || !strcmp(argv[1], "-")) {
		f = stdin;
		fname = "<stdin>";
	} else {
		f = fopen(argv[1], "r");
		if (f == NULL)
			err(1, "fopen");
		fname = argv[1];
	}

	expr = parse(f);
	if (expr == NULL)
		return 1;

	(void)fclose(f);

	ctx = ctx_new();
	if (ctx == NULL)
		err(1, NULL);

	res = eval_expr(ctx, expr, ctx->global);
	if (res == NULL) {
		fprintf(stderr, "Error: %s\n", ctx->err);
		return 1;
	}

	ctx_free(ctx);
	/* XXX: free code */

	printf("Result: ");
	print_obj(res);
	printf("\n");
	obj_unref(res);

	return 0;
}

struct obj *
scope(struct obj *parent)
{
	struct obj *s;

	s = object(OBJ_SCOPE, NULL);
	if (s == NULL)
		return NULL;

	RB_INIT(&s->data.scope.nodes);
	if (parent != NULL)
		obj_ref(parent);
	s->data.scope.parent = parent;
	return s;
}

int
scope_add(struct obj *s, const char *name, struct obj *obj)
{
	struct node *node, find;;

	find.name = (char *)name;
	node = RB_FIND(nodetree, &s->data.scope.nodes, &find);
	if (node != NULL) {
		obj_unref(node->obj);
		node->obj = obj_ref(obj);
		return 0;
	}

	node = malloc(sizeof(struct node));
	if (node == NULL)
		return -1;

	node->name = strdup(name);
	if (node->name == NULL) {
		free(node);
		return -1;
	}

	node->obj = obj_ref(obj);

	RB_INSERT(nodetree, &s->data.scope.nodes, node);
	return 0;
}

struct obj *
scope_lookup(struct obj *s, const char *name)
{
	struct node find, *node;

	find.name = (char *)name;
	for(; s != NULL; s = s->data.scope.parent) {
		node = RB_FIND(nodetree, &s->data.scope.nodes, &find);
		if (node != NULL)
			return obj_ref(node->obj);
	}
	return NULL;
}

struct obj *
object(enum objtype type, struct obj *proto)
{
	struct obj *obj;

	obj = malloc(sizeof(struct obj));
	if (obj == NULL)
		return NULL;

	obj->type = type;
	obj->ref = 1;
	RB_INIT(&obj->attrs);

	if (proto != NULL && obj_setattr(obj, "__proto__", proto) == -1) {
		obj_unref(obj);
		return NULL;
	}
	return obj;
}

void
obj_unref(struct obj *obj)
{
	if (obj == NULL || obj->ref == 0)
		return;
	if (--obj->ref == 0)
		obj_free(obj);
	else
		gc_collect(obj);
}

void
obj_free(struct obj *obj)
{
	struct node *node, *next;

	for (node = RB_MIN(nodetree, &obj->attrs); node != NULL; node = next) {
		next = RB_NEXT(nodetree, &obj->attrs, node);
		RB_REMOVE(nodetree, &obj->attrs, node);
		free(node->name);
		obj_unref(node->obj);
		free(node);
	}

	switch (obj->type) {
	case OBJ_STR:
		free(obj->data.str.buf);
		break;
	case OBJ_FUNC:
		obj_unref(obj->data.func.scope);
		break;
	case OBJ_SCOPE:
		obj_unref(obj->data.scope.parent);
		for (node = RB_MIN(nodetree, &obj->data.scope.nodes);
		    node != NULL; node = next) {
			next = RB_NEXT(nodetree, &obj->data.scope.nodes, node);
			RB_REMOVE(nodetree, &obj->data.scope.nodes, node);
			free(node->name);
			obj_unref(node->obj);
			free(node);
		}
		break;
	default:
		break;

	}
	free(obj);
}

int
obj_setattr(struct obj *obj, const char *name, struct obj *val)
{
	struct node find, *node;

	find.name = (char *)name;
	node = RB_FIND(nodetree, &obj->attrs, &find);
	if (node != NULL) {
		obj_unref(node->obj);
		node->obj = obj_ref(val);
		return 0;
	}

	node = malloc(sizeof(struct node));
	if (node == NULL)
		return -1;

	node->name = strdup(name);
	if (node->name == NULL) {
		free(node);
		return -1;
	}

	node->obj = obj_ref(val);
	RB_INSERT(nodetree, &obj->attrs, node);
	return 0;
}

struct obj *
obj_getattr(struct obj *obj, const char *name)
{
	struct node find, *node;

	for (;;) {
		find.name = (char *)name;
		node = RB_FIND(nodetree, &obj->attrs, &find);
		if (node != NULL)
			return obj_ref(node->obj);

		find.name = (char *)"__proto__";
		node = RB_FIND(nodetree, &obj->attrs, &find);
		if (node == NULL)
			break;
		obj = node->obj;
	}
	return NULL;
}

struct obj *
obj_callattr(struct ctx *ctx, struct obj *s, struct obj *obj,
    const char *attrname, int argc, struct expr **argv)
{
	struct obj *fn, *call_scope, *res = NULL, *a;
	int i;

	fn = obj_getattr(obj, attrname);
	if (fn == NULL) {
		return NULL;
	} else if (fn->type== OBJ_CFUNC) {
		res = fn->data.cfunc(ctx, s, obj, argc, argv);
		goto out;
	}  else if (fn->type != OBJ_FUNC) {
		ctx_seterr(ctx, "can't call non-function");
		goto out;
	}

	if (fn->data.func.arity != argc) {
		ctx_seterr(ctx,
		    "wrong number of arguments: expected %d, got %d",
		    fn->data.func.arity, argc);
		goto out;
	}

	call_scope = scope(fn->data.func.scope);
	if (call_scope == NULL ||
	    scope_add(call_scope, "this", obj) == -1) {
		obj_unref(call_scope);
		ctx_seterr(ctx, "%s", strerror(errno));
		goto out;
	}

	for (i = 0; i < argc; i++) {
		a = eval_expr(ctx, argv[i], s);
		if (a == NULL) {
			obj_unref(call_scope);
			goto out;
		}

		if (scope_add(call_scope, fn->data.func.argnames[i], a) == -1) {
			obj_unref(a);
			obj_unref(call_scope);
			ctx_seterr(ctx, "%s", strerror(errno));
			goto out;
		}
		obj_unref(a);
	}

	res = eval_expr(ctx, fn->data.func.code, call_scope);

	obj_unref(call_scope);
out:
	obj_unref(fn);
	return res;
}

void
print_obj(struct obj *obj)
{
	struct node *node;
	int i;

	if (obj == NULL) {
		printf("NULL");
		return;
	}

	switch (obj->type) {
	case OBJ_PLAIN:
		printf("obj");	/* XXX */
		break;
	case OBJ_NIL:
		printf("nil");
		break;
	case OBJ_BOOL:
		printf(obj->data.b ? "true" : "false");
		break;
	case OBJ_INT:
		printf("%d", obj->data.i);
		break;
	case OBJ_FLOAT:
		printf("%f", obj->data.f);
		break;
	case OBJ_STR:
		printf("\"%*s\"", (int)obj->data.str.len,
		    obj->data.str.buf);
		break;
	case OBJ_FUNC:
		printf("func(");
		for (i = 0; i < obj->data.func.arity; i++) {
			printf("%s%s", obj->data.func.argnames[i],
			    i == obj->data.func.arity - 1 ? "" : ", ");
		}
		printf(")");
		break;
	case OBJ_CFUNC:
		printf("function at %p", obj->data.cfunc);
		break;
	case OBJ_SCOPE:
		printf("(SCOPE ");	/* XXX */
		RB_FOREACH(node, nodetree, &obj->data.scope.nodes)
			printf(" %s", node->name);
		printf(")");
		break;
	}
}

RB_GENERATE(nodetree, node, entry, nodecmp);

int
nodecmp(struct node *n1, struct node *n2)
{
	return strcmp(n1->name, n2->name);
}

struct obj *
build_proto(const struct proto_op *proto_ops, struct obj *obj_proto)
{
	struct obj *proto, *fn;
	const struct proto_op *op;

	proto = object(OBJ_PLAIN, obj_proto);
	if (proto == NULL)
		return NULL;

	for (op = proto_ops; op != NULL && op->name != NULL; op++) {
		fn = object(OBJ_CFUNC, NULL);	/* XXX: use func_proto here? */
		if (fn == NULL) {
			obj_unref(proto);
			return NULL;
		}
		fn->data.cfunc = op->fn;
		if (obj_setattr(proto, op->name, fn) == -1) {
			obj_unref(fn);
			obj_unref(proto);
			return NULL;
		}
		obj_unref(fn);
	}
	return proto;
}

struct ctx *
ctx_new(void)
{
	struct ctx *ctx;

	ctx = calloc(1, sizeof(struct ctx));
	if (ctx == NULL)
		return NULL;

	if ((ctx->global = scope(NULL)) == NULL ||
	    (ctx->obj_proto = build_proto(obj_ops, NULL)) == NULL ||
	    (ctx->nil_proto = build_proto(NULL, ctx->obj_proto)) == NULL ||
	    (ctx->bool_proto = build_proto(bool_ops, ctx->obj_proto)) == NULL ||
	    (ctx->int_proto = build_proto(num_ops, ctx->obj_proto)) == NULL ||
	    (ctx->float_proto = build_proto(num_ops, ctx->obj_proto)) == NULL ||
	    (ctx->str_proto = build_proto(str_ops, ctx->obj_proto)) == NULL ||
	    (ctx->func_proto = build_proto(NULL, ctx->obj_proto)) == NULL ||
	    (ctx->nil_obj = object(OBJ_NIL, ctx->nil_proto)) == NULL ||
	    (ctx->true_obj = object(OBJ_BOOL, ctx->bool_proto)) == NULL ||
	    (ctx->false_obj = object(OBJ_BOOL, ctx->bool_proto)) == NULL) {
		ctx_free(ctx);
		return NULL;
	}

	ctx->true_obj->data.b = true;
	ctx->false_obj->data.b = false;

	return ctx;
}

void
ctx_free(struct ctx *ctx)
{
	obj_unref(ctx->global);
	obj_unref(ctx->obj_proto);
	obj_unref(ctx->nil_proto);
	obj_unref(ctx->bool_proto);
	obj_unref(ctx->int_proto);
	obj_unref(ctx->float_proto);
	obj_unref(ctx->str_proto);
	obj_unref(ctx->func_proto);
	obj_unref(ctx->nil_obj);
	obj_unref(ctx->true_obj);
	obj_unref(ctx->false_obj);
	free(ctx);
}

void
ctx_seterr(struct ctx *ctx, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	(void)vsnprintf(ctx->err, sizeof(ctx->err), fmt, ap);
	va_end(ap);
}

struct obj *
eval_expr(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	switch (expr->type) {
	case EXPR_NIL:
		return eval_nil(ctx, expr, s);
	case EXPR_BOOL:
		return eval_bool(ctx, expr, s);
	case EXPR_INT:
		return eval_int(ctx, expr, s);
	case EXPR_FLOAT:
		return eval_float(ctx, expr, s);
	case EXPR_STR:
		return eval_str(ctx, expr, s);
	case EXPR_IDENT:
		return eval_ident(ctx, expr, s);
	case EXPR_FUNC:
		return eval_func(ctx, expr, s);
	case EXPR_CALL:
		return eval_call(ctx, expr, s);
	case EXPR_BLOCK:
		return eval_block(ctx, expr, s);
	case EXPR_IFELSE:
		return eval_ifelse(ctx, expr, s);
	case EXPR_ASSIGN:
		return eval_assign(ctx, expr, s);
	case EXPR_NEW:
		return eval_new(ctx, expr, s);
	case EXPR_SETATTR:
		return eval_setattr(ctx, expr, s);
	case EXPR_GETATTR:
		return eval_getattr(ctx, expr, s);
	case EXPR_CALLATTR:
		return eval_callattr(ctx, expr, s);
	default:
		ctx_seterr(ctx, "unknown expression type %d", expr->type);
		return NULL;
	}
}

static struct obj *
eval_int(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj;

	obj = object(OBJ_INT, ctx->int_proto);
	if (obj == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		return NULL;
	}

	obj->data.i = expr->data.i;
	return obj;
}

static struct obj *
eval_float(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj;

	obj = object(OBJ_FLOAT, ctx->float_proto);
	if (obj == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		return NULL;
	}

	obj->data.f = expr->data.f;
	return obj;
}

static struct obj *
eval_str(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj;

	obj = object(OBJ_STR, ctx->str_proto);
	if (obj == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		return NULL;
	}

	obj->data.str.buf = malloc(expr->data.str.len);
	if (obj->data.str.buf == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		obj_unref(obj);
		return NULL;
	}
	memcpy(obj->data.str.buf, expr->data.str.buf, expr->data.str.len);
	obj->data.str.len = expr->data.str.len;
	return obj;
}

static struct obj *
eval_ident(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj;

	obj = scope_lookup(s, expr->data.ident);
	if (obj == NULL)
		ctx_seterr(ctx, "'%s' is not defined", expr->data.ident);
	return obj;
}

static struct obj *
eval_nil(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	return obj_ref(ctx->nil_obj);
}

static struct obj *
eval_bool(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	return obj_ref(expr->data.b ? ctx->true_obj : ctx->false_obj);
}

static struct obj *
eval_func(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj;

	obj = object(OBJ_FUNC, ctx->func_proto);
	if (obj == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		return NULL;
	}

	obj->data.func.arity = expr->data.func.arity;
	obj->data.func.argnames = expr->data.func.argnames;
	obj->data.func.code = expr->data.func.code;
	obj->data.func.scope = obj_ref(s);
	return obj;
}

static struct obj *
eval_call(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *fn, *call_scope, *res = NULL, *a;
	int i;

	fn = eval_expr(ctx, expr->data.call.func, s);
	if (fn == NULL) {
		return NULL;
	} else if (fn->type == OBJ_CFUNC) {
		res = fn->data.cfunc(ctx, s, NULL, expr->data.call.argc,
		    expr->data.call.argv);
		goto out;
	} else if (fn->type != OBJ_FUNC) {
		ctx_seterr(ctx, "can't call non-function");
		goto out;
	}

	if (fn->data.func.arity != expr->data.call.argc) {
		ctx_seterr(ctx,
		    "wrong number of arguments: expected %d, got %d",
		    fn->data.func.arity, expr->data.call.argc);
		goto out;
	}

	call_scope = scope(fn->data.func.scope);
	if (call_scope == NULL) {
		ctx_seterr(ctx, "%s", strerror(errno));
		goto out;
	}

	for (i = 0; i < expr->data.call.argc; i++) {
		a = eval_expr(ctx, expr->data.call.argv[i], s);
		if (a == NULL) {
			obj_unref(call_scope);
			goto out;
		}

		if (scope_add(call_scope, fn->data.func.argnames[i], a) == -1) {
			obj_unref(a);
			obj_unref(call_scope);
			ctx_seterr(ctx, "%s", strerror(errno));
			goto out;
		}
		obj_unref(a);
	}

	res = eval_expr(ctx, fn->data.func.code, call_scope);

	obj_unref(call_scope);
out:
	obj_unref(fn);
	return res;
}

static struct obj *
eval_block(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct expr *e;
	struct obj *res = NULL;

	TAILQ_FOREACH(e, &expr->data.block, entry) {
		if (res != NULL)
			obj_unref(res);
		res = eval_expr(ctx, e, s);
		if (res == NULL)
			return NULL;
	}
	return res;
}

static struct obj *
eval_ifelse(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *cond, *res = NULL;

	cond = eval_expr(ctx, expr->data.ifelse.cond, s);
	if (cond == NULL) {
		return NULL;
	} else if (cond->type != OBJ_BOOL) {
		ctx_seterr(ctx, "if condition must be a boolean");
		goto out;
	}

	if (cond->data.b)
		res = eval_expr(ctx, expr->data.ifelse.branch1, s);
	else if (expr->data.ifelse.branch2 != NULL)
		res = eval_expr(ctx, expr->data.ifelse.branch2, s);
	else
		res = obj_ref(ctx->nil_obj);

out:
	obj_unref(cond);
	return res;
}

static struct obj *
eval_assign(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *res;

	res = eval_expr(ctx, expr->data.assign.val, s);
	if (res == NULL)
		return NULL;

	if (scope_add(s, expr->data.assign.name, res) == -1) {
		ctx_seterr(ctx, "%s", strerror(errno));
		obj_unref(res);
		return NULL;
	}

	return res;
}

static struct obj *
eval_new(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	return object(OBJ_PLAIN, ctx->obj_proto);
}

static struct obj *
eval_setattr(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj, *res = NULL;

	obj = eval_expr(ctx, expr->data.setattr.obj, s);
	if (obj == NULL)
		return NULL;

	res = eval_expr(ctx, expr->data.setattr.val, s);
	if (res == NULL)
		goto out;

	if (obj_setattr(obj, expr->data.setattr.attrname, res) == -1) {
		obj_unref(res);
		res = NULL;
		ctx_seterr(ctx, "%s", strerror(errno));
	}

out:
	obj_unref(obj);
	return res;
}

static struct obj *
eval_getattr(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj, *res = NULL;

	obj = eval_expr(ctx, expr->data.getattr.obj, s);
	if (obj == NULL)
		return NULL;

	res = obj_getattr(obj, expr->data.getattr.attrname);
	if (res == NULL) {
		ctx_seterr(ctx, "object doesn't have attribute '%s'",
		    expr->data.getattr.attrname);
	}

	obj_unref(obj);
	return res;
}

static struct obj *
eval_callattr(struct ctx *ctx, struct expr *expr, struct obj *s)
{
	struct obj *obj, *res;

	obj = eval_expr(ctx, expr->data.callattr.obj, s);
	if (obj == NULL)
		return NULL;

	res = obj_callattr(ctx, s, obj, expr->data.callattr.attrname,
	    expr->data.callattr.argc, expr->data.callattr.argv);

	obj_unref(obj);
	return res;
}
