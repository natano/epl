/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* %option never-interactive */
%option yylineno
%option noyywrap
%option noinput
%option nounput

%{
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "y.tab.h"

int	yylex(void);
int	yyerror(const char *, ...);

static int	parse_int(const char *, int);
static float	parse_float(const char *);
%}

%%
#.*	/* ignore comment */
[ \t\n]	/* ignore whitespace */

"=="			return OP_EQ;
"!="			return OP_NE;
[-+*/%(){},.;?:=]	return yytext[0];

[1-9][0-9]*	{ yylval.i = parse_int(yytext, 10); return INT; }
0b[01]+		{ yylval.i = parse_int(yytext + 2, 2); return INT; }
0[0-7]*		{ yylval.i = parse_int(yytext, 8); return INT; }
0x[0-9a-fA-F]+	{ yylval.i = parse_int(yytext, 16); return INT; }

[0-9]*\.[0-9]+([Ee][+-]?[0-9]+)? {
	yylval.f = parse_float(yytext);
	return FLOAT;
}

\"[^"]*\"	{
	/* XXX: handling for \n & co. */
	yylval.str.buf = malloc(yyleng - 2);
	if (yylval.str.buf == NULL)
		yyerror("%s", strerror(errno));
	memcpy(yylval.str.buf, yytext + 1, yyleng - 2);
	yylval.str.len = yyleng - 2;
	return STR;
}

[a-zA-Z_][a-zA-Z_0-9]* {
	if (strcmp(yytext, "nil") == 0) {
		return KW_NIL;
	} else if (strcmp(yytext, "true") == 0) {
		return KW_TRUE;
	} else if (strcmp(yytext, "false") == 0) {
		return KW_FALSE;
	} else if (strcmp(yytext, "inf") == 0) {
		yylval.f = INFINITY;
		return FLOAT;
	} else if (strcmp(yytext, "nan") == 0) {
		yylval.f = NAN;
		return FLOAT;
	} else if (strcmp(yytext, "func") == 0) {
		return KW_FUNC;
	} else if (strcmp(yytext, "if") == 0) {
		return KW_IF;
	} else if (strcmp(yytext, "else") == 0) {
		return KW_ELSE;
	} else if (strcmp(yytext, "new") == 0) {
		return KW_NEW;
	}

	yylval.ident = strdup(yytext);
	if (yylval.ident == NULL)
		yyerror("%s", strerror(errno));
	return IDENT;
}

.	yyerror("illegal token");

%%

static int
parse_int(const char *s, int base)
{
	long l;

	errno = 0;
	l = strtol(s, NULL, base);
	if ((errno == ERANGE && (l == LONG_MIN || l == LONG_MAX)) ||
	    (l < INT_MIN || l > INT_MAX)) {
		yyerror("integer out of range");
	}

	return (int)l;

}

static float
parse_float(const char *s)
{
	float f;

	errno = 0;
	f = strtof(s, NULL);
	if (errno == ERANGE && (f == -HUGE_VALF || f == HUGE_VALF))
		yyerror("float out of range");

	return f;
}
