/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef EXPR_H
#define EXPR_H

enum etype {
	EXPR_NIL, EXPR_BOOL, EXPR_INT, EXPR_FLOAT, EXPR_STR, EXPR_IDENT,
	EXPR_FUNC, EXPR_CALL, EXPR_BLOCK, EXPR_IFELSE, EXPR_ASSIGN,
	EXPR_NEW, EXPR_SETATTR, EXPR_GETATTR, EXPR_CALLATTR,
};

TAILQ_HEAD(exprlist, expr);

struct expr {
	enum etype	type;
	union {
		_Bool	 b;
		int	 i;
		float	 f;
		struct {
			char	*buf;
			size_t	 len;
		} str;
		char	*ident;
		struct {
			int		 arity;
			char		**argnames;
			struct expr	*code;
		} func;
		struct {
			struct expr	*func;
			int		 argc;
			struct expr	**argv;
		} call;
		struct {
			struct expr	*cond;
			struct expr	*branch1;
			struct expr	*branch2;
		} ifelse;
		struct {
			char		*name;
			struct expr	*val;
		} assign;
		struct {
			struct expr	*obj;
			char		*attrname;
		} getattr;
		struct {
			struct expr	*obj;
			char		*attrname;
			struct expr	*val;
		} setattr;
		struct {
			struct expr	*obj;
			char		*attrname;
			int		 argc;
			struct expr	**argv;
		} callattr;
		struct exprlist block;
	} data;

	TAILQ_ENTRY(expr) entry;
};

#endif /* !EXPR_H */
