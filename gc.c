/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/tree.h>

#include <stdio.h>
#include <string.h>

#include "epl.h"

RB_HEAD(gctree, obj);

static int objcmp(struct obj *, struct obj *);
RB_PROTOTYPE_STATIC(gctree, obj, gc_entry, objcmp);
RB_GENERATE_STATIC(gctree, obj, gc_entry, objcmp);

static int
objcmp(struct obj *o1, struct obj *o2)
{
	return memcmp(&o1, &o2, sizeof(struct obj *));
}

/*
 * Clean up cyclic references.
 * The algorithm implemented below determines reachability of an object by
 * observing its reference count on the supposition that itself doesn't hold
 * any references (like it doesn't exist). If the refcount drops to zero, the
 * object can be freed.
 */
void
gc_collect(struct obj *target)
{
	struct gctree visited = RB_INITIALIZER(&visited);
	struct gctree candidates = RB_INITIALIZER(&candidates);
	struct obj *obj;
	struct node *node;

	target->gc_ref = target->ref;
	RB_INSERT(gctree, &candidates, target);

#define VISIT(x)	do {					\
	if (RB_FIND(gctree, &visited, (x)) != NULL) {		\
		if (--(x)->gc_ref == 0) {			\
			RB_REMOVE(gctree, &visited, (x));	\
			RB_INSERT(gctree, &candidates, (x));	\
		}						\
	} else {						\
		(x)->gc_ref = (x)->ref - 1;			\
		if ((x)->gc_ref == 0)				\
			RB_INSERT(gctree, &candidates, (x));	\
		else						\
			RB_INSERT(gctree, &visited, (x));	\
	}							\
} while (0)

	for (obj = target; obj != NULL && target->gc_ref > 0;
	    obj = RB_MIN(gctree, &candidates)) {
		RB_FOREACH(node, nodetree, &obj->attrs)
			VISIT(node->obj);

		switch (obj->type) {
		case OBJ_FUNC:
			VISIT(obj->data.func.scope);
			break;
		case OBJ_SCOPE:
			RB_FOREACH(node, nodetree, &obj->data.scope.nodes)
				VISIT(node->obj);
			if (obj->data.scope.parent != NULL)
				VISIT(obj->data.scope.parent);
			break;
		default:
			break;
		}

		RB_REMOVE(gctree, &candidates, obj);
		RB_INSERT(gctree, &visited, obj);
	}

	if (target->gc_ref == 0) {
		target->ref = 0;
		obj_free(target);
	}
}
