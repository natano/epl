/*
 * Copyright (c) 2014-2015 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/tree.h>

#include <stdbool.h>
#include <stdio.h>

#include "epl.h"
#include "expr.h"

static struct obj	*bool_eq(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);
static struct obj	*bool_ne(struct ctx *, struct obj *, struct obj *,
			    int, struct expr **);

const struct proto_op bool_ops[] = {
	{"__eq__", bool_eq},
	{"__ne__", bool_ne},
	{NULL, NULL},
};

#define COMPARISON(name, op)						\
static struct obj *							\
bool_##name(struct ctx *ctx, struct obj *s, struct obj *obj, int argc,	\
    struct expr **argv)							\
{									\
	struct obj *other;						\
	bool ret;							\
									\
	if (obj == NULL || obj->type != OBJ_BOOL) {			\
		ctx_seterr(ctx, "expected bool");			\
		return NULL;						\
	} else if (argc != 1) {						\
		ctx_seterr(ctx, "wrong number of arguments");		\
		return NULL;						\
	}								\
									\
	other = eval_expr(ctx, argv[0], s);				\
	if (other == NULL)						\
		return NULL;						\
									\
	if (obj->type == OBJ_BOOL)					\
		ret = obj->data.b op other->data.b;			\
	else								\
		ret = true op true;					\
									\
	obj_unref(other);						\
	return obj_ref(ret ? ctx->true_obj : ctx->false_obj);		\
}

COMPARISON(eq, ==)
COMPARISON(ne, !=)
